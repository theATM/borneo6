package net.stawrul.services.exceptions;

/**
 * Wyjątek sygnalizujący brak jakichkolwiek booków w orderze (order jest pusty)
 *
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback)....
 */
public class BadOrderException extends RuntimeException {
}
