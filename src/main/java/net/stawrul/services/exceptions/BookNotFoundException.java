package net.stawrul.services.exceptions;

/**
 * Wyjątek sygnalizujący nie znalezienie szukanego booka w bazie
 *
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback).
 */
public class BookNotFoundException extends RuntimeException {}
