package net.stawrul.services.exceptions;

/**
 * Wyjątek sygnalizujący niespełnienie przez order wymagań minimalnych
 * Czyli sumaryczna cema wszystkich książek jest za mała
 * Wystąpienie wyjątku z hierarchii RuntimeException w warstwie biznesowej
 * powoduje wycofanie transakcji (rollback).
 */
public class ToSmallOrderException extends RuntimeException {}
