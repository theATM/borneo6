package net.stawrul.services;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.exceptions.OutOfStockException;
import net.stawrul.services.exceptions.BadOrderException;
import net.stawrul.services.exceptions.ToSmallOrderException;
import net.stawrul.services.exceptions.BookNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Komponent (serwis) biznesowy do realizacji operacji na zamówieniach.
 */
@Service
public class OrdersService extends EntityService<Order>
{
    private final int MIN_PRICE = 200;// minimalna cena orderu
    //Instancja klasy EntityManger zostanie dostarczona przez framework Spring
    //(wstrzykiwanie zależności przez konstruktor).
    public OrdersService(EntityManager em)
    {

        //Order.class - klasa encyjna, na której będą wykonywane operacje
        //Order::getId - metoda klasy encyjnej do pobierania klucza głównego
        super(em, Order.class, Order::getId);
    }

    /**
     * Pobranie wszystkich zamówień z bazy danych.
     *
     * @return lista zamówień
     */
    public List<Order> findAll() {
        return em.createQuery("SELECT o FROM Order o", Order.class).getResultList();
    }

    /**
     * Złożenie zamówienia w sklepie.
     * <p>
     * Zamówienie jest akceptowane, jeśli wszystkie objęte nim produkty są dostępne (przynajmniej 1 sztuka). W wyniku
     * złożenia zamówienia liczba dostępnych sztuk produktów jest zmniejszana o jeden. Metoda działa w sposób
     * transakcyjny - zamówienie jest albo akceptowane w całości albo odrzucane w całości. W razie braku produktu
     * wyrzucany jest wyjątek OutOfStockException.
     *
     * @param order zamówienie do przetworzenia
     */
    @Transactional
    public void placeOrder(Order order)
    {
        order.setPrice(0);
        //czy order jest pusty?
        if(order.getBooks().size() == 0) throw new BadOrderException();
        //czy jest warty zachodu -> tzn czy price jest większa niż MIN_PRICE
        int price = 0;
        // + dodawanie książek
        for (Book bookStub : order.getBooks())
        {
            Book book = em.find(Book.class, bookStub.getId());
            if(book ==  null) throw new BookNotFoundException(); //nie znaleziono danej pozycji w bazie
            if (book.getAmount() < 1) {
                //wyjątek z hierarchii RuntineException powoduje wycofanie transakcji (rollback)
                throw new OutOfStockException();
            }
            else {
                int newAmount = book.getAmount() - 1;
                book.setAmount(newAmount);
                price += book.getPrice();
            }
        }
        if(price < MIN_PRICE) throw new ToSmallOrderException();
        order.setPrice(price);
        //jeśli wcześniej nie został wyrzucony wyjątek OutOfStockException lub BadOrderException
        // , zamówienie jest zapisywane w bazie danych
        save(order);
    }
}
