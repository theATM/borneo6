package net.stawrul;

import net.stawrul.model.Book;
import net.stawrul.model.Order;
import net.stawrul.services.OrdersService;
import net.stawrul.services.exceptions.OutOfStockException;
import net.stawrul.services.exceptions.BadOrderException;
import net.stawrul.services.exceptions.ToSmallOrderException;
import net.stawrul.services.exceptions.BookNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.persistence.EntityManager;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class OrdersServiceTest
{

    @Mock
    EntityManager em;


    //przetestować obsługę sytuacji wyjątkowych
    @Test(expected = BadOrderException.class)
    public void whenOrderIsEmpty_placeOrderThrowsBadOrderException()
    {
        //Arrange
        Order order = new Order();
        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected BadOrderException()
    }

    //przetestować obsługę sytuacji wyjątkowych
    @Test(expected = OutOfStockException.class)
    public void whenOrderedBookNotAvailable_placeOrderThrowsOutOfStockEx()
    {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(0);
        book.setPrice(300); // By była większa niż minimalna wymagana
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected OutOfStockEx()
    }

    //poprawne	 dane
    //wejściowe	skutkujące	poprawnym	rezultatem	działania
    @Test
    public void whenOrderedBookAvailable_placeOrderDecreasesAmountByOne()
    {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        book.setPrice(300); // By była większa niż minimalna wymagana
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //dostępna liczba książek zmniejszyła się:
        assertEquals(0, (int)book.getAmount());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }

    //przetestować obsługę sytuacji wyjątkowych
    @Test(expected = ToSmallOrderException.class)
    public void whenSingleBookInOrderIsWorthTooLittle_orderThrowsToSmallOrderException()
    {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        book.setPrice(100); // By była mniejsza niż minimalna wymagana
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected ToSmallOrderException()
    }

    //przetestować	obsługę	sytuacji wyjątkowych
    @Test(expected = ToSmallOrderException.class)
    public void whenManyBooksInOrderAreWorthTooLittle_orderThrowsToSmallOrderException()
    {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(3);
        book.setPrice(30); // By była mniejsza niż minimalna wymagana (90 < 200)
        order.getBooks().add(book);

        Mockito.when(em.find(Book.class, book.getId())).thenReturn(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected ToSmallOrderException()
    }


    //poprawne	 dane
    //wejściowe	skutkujące	poprawnym	rezultatem	działania
    @Test
    public void whenOrderHasManyExpensiveBooks_WholePriceShouldBeSumOfAllPrices()
    {
        //Arrange
        // cena każdego booka przekracza minimalną cenę zamównienia
        Order order = new Order();
        Book book1 = new Book();
        book1.setAmount(1);
        book1.setPrice(300);
        order.getBooks().add(book1);

        Book book2 = new Book();
        book2.setAmount(1);
        book2.setPrice(300);
        order.getBooks().add(book2);

        Book book3 = new Book();
        book3.setAmount(1);
        book3.setPrice(300);
        order.getBooks().add(book3);


        Mockito.when(em.find(Book.class, book1.getId())).thenReturn(book1);
        Mockito.when(em.find(Book.class, book2.getId())).thenReturn(book2);
        Mockito.when(em.find(Book.class, book3.getId())).thenReturn(book3);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert
        //Calowita cena orderu powinna być sumą cen wszystkich booków:
        assertEquals(900, (int)order.getPrice());
        //nastąpiło dokładnie jedno wywołanie em.persist(order) w celu zapisania zamówienia:
        Mockito.verify(em, times(1)).persist(order);
    }

    //przetestować obsługę sytuacji wyjątkowych
    @Test(expected = BookNotFoundException.class)
    public void whenSingleBookInOrderIsNotInDataBase_orderThrowsBookNotFoundException()
    {
        //Arrange
        Order order = new Order();
        Book book = new Book();
        book.setAmount(1);
        book.setPrice(300); // By była większa niż minimalna wymagana
        book.setTitle("NieMaTakiego");
        order.getBooks().add(book);

        OrdersService ordersService = new OrdersService(em);

        //Act
        ordersService.placeOrder(order);

        //Assert - exception expected BookNotFoundException()
    }

}
